# SqlKata.ModelHelper

ModelHelper provides easy connect from your model classes to database by using SqlKata for connecting database and running your queries.

Supported on .Net Core

#### Installation

Add [Nuget Package](https://www.nuget.org/packages/SqlKata.ModelHelper/) from your package manager.

```
SqlKata.ModelHelper
```

#### Usage

##### Adding QueryFactory

Add a transient for QueryFactory with your database informations.

```csharp
 services.AddTransient(f =>
 {
     return new QueryFactory
     {
         Compiler = new PostgresCompiler(),
         Connection = new NpgsqlConnection("server=.;database=your_database;user=your_user;pass=your_pass;")
     };
 });
```

##### Define Entity

```csharp
[Table("account.user")]
public class UserDto : ITable, IFilterObject
{
    [PrimaryKey]
    public int Id { get; set; }
    public string Email { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
}
```

##### Define Data Access

```csharp
public class UserService : DataServiceBase<UserDto, UserDto>
{
    public UserService(QueryFactory d, ILogger<UserService> logger) : base(d, logger)
    {
    }
}
```

#### Methods

```csharp
public class FooManager
{
    private readonly UserService _userService;

    public FooManager(UserService userService)
    {
        _userService = userService;
    }

    public async Task Insert()
    {
        UserDto data = await _userService.Insert(new UserDto
        {
            FirstName = "first name",
            LastName = "last name",
            Email = "foo@foo.com",
        });
    }

    public async Task Update()
    {
        UserDto data = await _userService.Update(new UserDto
        {
            Id = 1, //Primary Key required
            FirstName = "Updated first name",
            LastName = "last name",
            Email = "foo@foo.com",
        });


        //also you can use `updateOnly` parameter for updating only specified field or fields
        UserDto data2 = await _userService.Update(new UserDto
        {
            Id = 1, //Primary Key required
            FirstName = "Updated first name"
        }, "FirstName");
    }

    public async Task Delete()
    {
        UserDto deleted = await _userService.Delete(new UserDto
        {
            Id = 1, //Primary Key required
        });
    }

    public async Task GetById()
    {
        UserDto data = await _userService.GetById(new UserDto
        {
            Id = 1
        });
    }

    public async Task GetFirstOrDefault()
    {
        UserDto data = await _userService.GetFirstOrDefault(new UserDto
        {
            Email = "foo@foo.com",
        });
    }

    public async Task Any()
    {
        bool isAny = await _userService.Any(new UserDto
        {
            Email = "foo@foo.com",
        });
    }

    public async Task<IEnumerable<UserDto>> Get()
    {
        DataResponse<UserDto> response = await _userService.Get(new DataRequest<UserDto>
        {
            Filter = new UserDto
            {
                TypeId = 2
            },
            Limit = 10, //pager limit
            Page = 1 //pager
        }, new SelectQueryOptions
        {
            ValuesForFilter = true
        });

        //total records
        Console.WriteLine(response.RowCount);

        //total pages
        Console.WriteLine(response.Page);


        return response.Data;
    }
}
```
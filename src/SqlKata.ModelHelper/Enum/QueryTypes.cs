﻿namespace SqlKata.ModelHelper
{
    public enum QueryTypes
    {
        All = 0,
        Select = 1,
        Insert = 2,
        Update = 3,
        Delete = 4,
        Any = 5,
        GetById = 6
    }
}

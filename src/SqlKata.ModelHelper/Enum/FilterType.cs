﻿namespace SqlKata.ModelHelper
{
    public enum FilterType
    {
        DateRange,
        IntRange,
        DecimalRange,
        OrNull,
        Equal,
        Ignore,
        GratherThan,
        LowerThan,
        CustomDbType
    }
}

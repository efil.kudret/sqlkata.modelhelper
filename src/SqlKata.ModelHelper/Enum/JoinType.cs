﻿namespace SqlKata.ModelHelper
{
    public enum JoinType
    {
        Inner,
        Left,
        Right
    }
}

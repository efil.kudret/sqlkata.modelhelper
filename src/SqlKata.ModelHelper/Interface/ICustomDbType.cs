﻿namespace SqlKata.ModelHelper
{
    public interface ICustomDbType
    {
        string GetValue();
    }
}

﻿namespace SqlKata.ModelHelper
{
    public interface ITableHasPrimary<T> : ITable
    {
        T Id { get; set; }
    }
}

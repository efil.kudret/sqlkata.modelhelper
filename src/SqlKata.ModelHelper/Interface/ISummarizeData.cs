namespace SqlKata.ModelHelper
{
    public interface ISummarizeData
    {
        public Query GetSummarizeQuery(Query baseQuery);
    }
}
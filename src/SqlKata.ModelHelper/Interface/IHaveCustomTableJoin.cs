﻿namespace SqlKata.ModelHelper
{
    public interface IHaveCustomTableJoin
    {
        Query GetCustomTableJoins(Query query);
    }
}

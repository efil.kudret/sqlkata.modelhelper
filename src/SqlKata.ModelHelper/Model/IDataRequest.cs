namespace SqlKata.ModelHelper.Model
{
    public interface IDataRequest
    {
        int? Page { get; set; }
        int? Limit { get; set; }
        string OrderKey { get; set; }
        bool? OrderDesc { get; set; }
        bool AllData { get; set; }
    }
}
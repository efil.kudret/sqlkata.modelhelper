namespace SqlKata.ModelHelper.Model
{
    public interface IDataResponse
    {
        long RowCount { get; set; }
        int Page { get; set; }
        int PerPage { get; set; }
        int TotalPages { get; set; }
        bool IsFirst { get; set; }
        bool IsLast { get; set; }
        bool HasNext { get; set; }
        bool HasPrevious { get; set; }
    }
}
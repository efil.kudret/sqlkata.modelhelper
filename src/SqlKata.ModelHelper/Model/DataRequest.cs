﻿namespace SqlKata.ModelHelper.Model
{
    using System;
    
    public class DataRequest<T> : IDataRequest
    {
        public string Language { get; set; }
        public int? Page { get; set; }

        public int? Limit { get; set; }

        public T Filter { get; set; }
        public string OrderKey { get; set; }
        public bool? OrderDesc { get; set; }
        public bool AllData { get; set; }

        public DataRequest<T> CloneFor<T>()
        {
            return new DataRequest<T>
            {
                Filter = Activator.CreateInstance<T>(),
                Limit = this.Limit,
                Page = this.Page,
                AllData = this.AllData,
                OrderDesc = this.OrderDesc,
                OrderKey = this.OrderKey
            };
        }
    }
}
﻿namespace SqlKata.ModelHelper.Model
{
    using System.Collections.Generic;

    public class DataResponse<T> : IDataResponse
    {
        public IEnumerable<T> Data { get; set; }
        public long RowCount { get; set; }
        public int Page { get; set; }
        public int PerPage { get; set; }
        public int TotalPages { get; set; }
        public bool IsFirst { get; set; }
        public bool IsLast { get; set; }
        public bool HasNext { get; set; }
        public bool HasPrevious { get; set; }
        public T Summary { get; set; }

        public DataResponse<T> CloneFor<T>()
        {
            return new DataResponse<T>
            {
                Page = Page,
                HasNext = HasNext,
                HasPrevious = HasPrevious,
                IsFirst = IsFirst,
                IsLast = IsLast,
                PerPage = PerPage,
                RowCount = RowCount,
                TotalPages = TotalPages
            };
        }
        
    }
}
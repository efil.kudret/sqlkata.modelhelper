using System;
using Microsoft.Extensions.DependencyInjection;

namespace SqlKata.ModelHelper
{
    public static class DataActivityLoggerExtensions
    {
        public static void UseActivityLogger(this IServiceCollection services, Action<ActivityInfo<ITable>> model)
        {
               
        }
    }

    public class ActivityInfo<T>
    {
        public short Operation { get; set; }
        public T Model { get; set; }
    }
}
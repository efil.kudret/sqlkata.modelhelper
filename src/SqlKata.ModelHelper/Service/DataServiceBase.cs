﻿namespace SqlKata.ModelHelper.Service
{
    using System;
    using System.Threading.Tasks;
    using NRTech.Toolkit;
    using Execution;
    using Microsoft.Extensions.Logging;
    using Model;
    using Order;

    public abstract class DataServiceBase<TModel, TFilterModel>
        where TModel : ITable
        where TFilterModel : IFilterObject, ITable
    {
        internal readonly ILogger<DataServiceBase<TModel, TFilterModel>> Logger;
        private readonly QueryFactory _db;

        public QueryFactory Factory => _db;

        public virtual OrderInfo DefaultOrder { get; }

        public virtual Query BeforeGet(Query q)
        {
            return q;
        }

        public DataServiceBase(QueryFactory d, ILogger<DataServiceBase<TModel, TFilterModel>> logger)
        {
            _db = d;
            Logger = logger;
        }

        public async Task<DataResponse<TModel>> Get(DataRequest<TFilterModel> request,
            SelectQueryOptions options = null)
        {
            try
            {
                if (request.Filter is null)
                    request.Filter = Activator.CreateInstance<TFilterModel>();

                if (request.Page.IsNull())
                    request.Page = 1;
                if (request.Limit.IsNull())
                    request.Limit = 10;

                if (options is null)
                    options = new SelectQueryOptions() { };

                ITable selectModel;
                if (typeof(TModel) == typeof(TFilterModel))
                    selectModel = request.Filter;
                else
                    selectModel = Activator.CreateInstance<TModel>();


                if (DefaultOrder.IsNotNull())
                {
                    request.OrderKey = DefaultOrder.OrderKey;
                    request.OrderDesc = DefaultOrder.Desc;
                }

                var orderInfo =
                    request.OrderKey.IsNotNull()
                        ? new OrderInfo(request.OrderKey, request.OrderDesc.GetValueOrDefault(false))
                        : DefaultOrder;
                
                var query = selectModel.GetSelectQuery(request.Filter, orderInfo, options);
                if (request.AllData)
                    return new DataResponse<TModel>
                    {
                        Data = await _db.FromQuery(query).GetAsync<TModel>()
                    };
                
                var summarized = default(TModel);
                if (selectModel is ISummarizeData)
                {
                    var sq = (selectModel as ISummarizeData).GetSummarizeQuery(query);
                    summarized = await _db.FromQuery(sq).FirstOrDefaultAsync<TModel>();
                } 

                if (options.OnlyCount)
                {
                    var res = await _db.FromQuery(query).CountAsync<int>();
                    return new DataResponse<TModel>
                    {
                        RowCount = res
                    };
                }

                var resp = await _db.FromQuery(query)
                    .PaginateAsync<TModel>(request.Page.Value, request.Limit.Value);
                return new DataResponse<TModel>
                {
                    Data = resp.List,
                    Page = resp.Page,
                    HasPrevious = resp.HasPrevious,
                    PerPage = resp.PerPage,
                    IsFirst = resp.IsFirst,
                    HasNext = resp.HasNext,
                    TotalPages = resp.TotalPages,
                    IsLast = resp.IsLast,
                    RowCount = resp.Count,
                    Summary = summarized
                };
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "ExceptionOnGet");
                throw ex;
            }
        }

        public virtual async Task<TModel> GetById(TFilterModel dto, SelectQueryOptions options = null)
        {
            if (options is null)
                options = new SelectQueryOptions() { ForGetById = true };

            options.ForGetById = true;
            var query = dto.GetSelectQuery(options);

            return await _db.FromQuery(query).FirstOrDefaultAsync<TModel>();
        }

        public virtual async Task<TModel> GetFirstOrDefault(TFilterModel dto, SelectQueryOptions options = null)
        {
            if (options is null)
                options = new SelectQueryOptions() { FiltersForEqual = true, ValuesForFilter = true };

            var mainDto = Activator.CreateInstance<TModel>();

            var query = mainDto.GetSelectQuery(dto, null, options);
            return await _db.FromQuery(query).FirstOrDefaultAsync<TModel>();
        }

        public virtual async Task<bool> Any(TFilterModel dto, SelectQueryOptions options = null)
        {
            if (options is null)
                options = new SelectQueryOptions { ValuesForFilter = true, FiltersForEqual = true };

            options.ForAny = true;

            var query = dto.GetSelectQuery(options);
            var count = await _db.FromQuery(new Query().From(query, "sq")).CountAsync<int>(new string[] { "*" });
            return count > 0;
        }

        public virtual async Task<TModel> Insert(TModel dto)
        {
            try
            {
                var query = dto.GetInsertQuery();
                var pKey = dto.GetPrimaryKey();

                var returnId = pKey.IsNotNull() && pKey.AutoIncrement;


                int checkNumber;
                if (returnId)
                {
                    checkNumber = query.CompileAndReturnId<int>(_db, pKey.IsNotNull() ? pKey.Underscored : null);
                    if (pKey.IsNotNull())
                        pKey.SetValue(dto, checkNumber);
                }
                else
                    checkNumber = _db.Execute(query);

                if (checkNumber > 0)
                {
                    if (returnId && dto is ITableHasPrimary<int>)
                        (dto as ITableHasPrimary<int>).Id = checkNumber;

                    return dto;
                }
                else
                    throw new Exception("Data couldn't insert.");
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "ExceptionOnInsert");
                throw ex;
            }
        }

        public virtual async Task<TModel> Update(TModel dto, params string[] updateOnly)
        {
            try
            {
                var query = dto.GetUpdateQuery(updateOnly);
                var result = await _db.ExecuteAsync(query);
                return dto;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "ExceptionOnUpdate");
                throw new Exception("Data couldn't update");
            }
        }

        public virtual async Task<TModel> Delete(TModel dto)
        {
            try
            {
                var query = dto.GetDeleteQuery();
                await _db.ExecuteAsync(query);
                return dto;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "ExceptionOnDelete");
                throw ex;
            }
        }
    }
}
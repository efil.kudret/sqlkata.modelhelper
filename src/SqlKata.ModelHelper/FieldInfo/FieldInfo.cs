﻿namespace SqlKata.ModelHelper
{
    using NRTech.Toolkit;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Linq;

    public class FieldInfo
    {
        public string PropertyName { get; set; }

        public string Underscored
        {
            get { return this.PropertyName.ToUnderscoreCase(); }
        }

        public object Value { get; set; }
        public bool AutoIncrement { get; set; }
        public bool PrimaryKey { get; set; }
        public bool IsNull { get; set; }
        public bool IsDefault { get; set; }
        public bool IsFilterType
        {
            get { return !Filter.IsNull(); }
        }

        public FilterPropertyAttribute Filter { get; set; }
        public string Prefix { get; set; }
        public TableJoinAttribute TableJoin { get; set; }
        public DataColumnAttribute DataColumn { get; set; }
        private PropertyInfo PropInfo { get; set; }

        public IgnorePropertyAttribute QueryIgnore { get; set; }

        public AlwaysInQueryAttribute AlwaysAttr { get; set; }

        public bool AlwaysInQuery => AlwaysAttr.IsNotNull();

        public string FieldName
        {
            get
            {
                var generated = !string.IsNullOrEmpty(this.Prefix)
                    ? string.Format("{0}.{1}", this.Prefix, this.Underscored)
                    : this.Underscored;
                if (this.DataColumn.IsNull())
                    return generated;
                else
                    return this.DataColumn.ColumnName.IsNotNull() ? this.DataColumn.ColumnName : generated;
            }
        }

        public string FieldNameWithoutAlias => FieldName.IsNull() ? "" : FieldName.ToLowerInvariant().Split(" as ").FirstOrDefault();

        public string FieldNameOnly => FieldNameWithoutAlias.ToLowerInvariant().Split(".").LastOrDefault();

        public static IEnumerable<FieldInfo> GetAllFields(object obj)
        {
            var mainAttr = obj.GetType().GetCustomAttribute<TableAttribute>();
            foreach (var item in obj.GetType().GetProperties().Where(x => !x.PropertyType.IsArray))
            {
                var attr = item.GetCustomAttribute<PrimaryKeyAttribute>();
                var filterAttr = item.GetCustomAttribute<FilterPropertyAttribute>();
                var joinAttr = item.GetCustomAttribute<TableJoinAttribute>();
                var dataColumnAttr = item.GetCustomAttribute<DataColumnAttribute>();
                var ignoreAttr = item.GetCustomAttribute<IgnorePropertyAttribute>();
                var alwaysAttr = item.GetCustomAttribute<AlwaysInQueryAttribute>();

                var value = item.GetValue(obj);
                yield return new FieldInfo
                {
                    PropertyName = item.Name,
                    Value = value,
                    AutoIncrement = attr?.AutoIncrement ?? false,
                    PrimaryKey = !(attr is null),
                    IsNull = value.IsNull(),
                    IsDefault = value.IsDefault(),
                    Filter = filterAttr,
                    Prefix = mainAttr.Alias,
                    TableJoin = joinAttr,
                    DataColumn = dataColumnAttr,
                    PropInfo = item,
                    QueryIgnore = ignoreAttr,
                    AlwaysAttr = alwaysAttr,
                    
                };
            }
        }

        public void SetValue(object obj, object value)
        {
            this.PropInfo.SetValue(obj, value);
        }

        public bool IgnoredFor(QueryTypes queryType)
        {
            if (QueryIgnore.IsNull())
                return false;
            else if (!QueryIgnore.All && !QueryIgnore.Select && !QueryIgnore.Insert && !QueryIgnore.Update &&
                     !QueryIgnore.Delete && !QueryIgnore.Any && !QueryIgnore.GetById)
                return true;
            else
                switch (queryType)
                {
                    default:
                    case QueryTypes.All:
                        return true;
                    case QueryTypes.Select:
                        return QueryIgnore.Select;
                    case QueryTypes.Insert:
                        return QueryIgnore.Insert;
                    case QueryTypes.Update:
                        return QueryIgnore.Update;
                    case QueryTypes.Delete:
                        return QueryIgnore.Delete;
                    case QueryTypes.Any:
                        return QueryIgnore.Any;
                    case QueryTypes.GetById:
                        return QueryIgnore.GetById;
                }
        }

        public bool SelectRaw
        {
            get
            {
                if (this.DataColumn.IsNotNull())
                    return this.DataColumn.SelectRaw;
                else
                    return false;
            }
        }
    }
}
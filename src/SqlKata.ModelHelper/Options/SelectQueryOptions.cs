﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SqlKata.ModelHelper
{
    public class SelectQueryOptions
    {
        public bool HasDeletedField { get; set; }
        public bool ForGetById { get; set; }
        public bool ValuesForFilter { get; set; }
        public bool ForAny { get; set; }
        public bool UseOrWhere { get; set; }
        public bool FiltersForEqual { get; set; }

        public bool OnlyCount { get; set; }
    }
}

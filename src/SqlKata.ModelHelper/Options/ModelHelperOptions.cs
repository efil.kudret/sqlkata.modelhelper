namespace SqlKata.ModelHelper
{
    public static class LoggerOptions
    {
        public static bool SqlQueryLogs { get; set; }
    }
}
﻿namespace SqlKata.ModelHelper
{
    using System;

    [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public sealed class DataColumnAttribute : Attribute
    {
        public string ColumnName { get; }
        public bool Foreign { get; set; }
        public bool SelectRaw { get; set; }
        
        public DataColumnAttribute(string columnName, bool foreign = false)
        {
            this.ColumnName = columnName;
            this.Foreign = foreign;
        }

        public DataColumnAttribute()
        {

        }
    }
}

using System;

namespace SqlKata.ModelHelper
{
    [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public sealed class AlwaysInQueryAttribute : Attribute
    {
        
    }
}
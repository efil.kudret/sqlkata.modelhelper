﻿namespace SqlKata.ModelHelper
{
    using System;

    [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public sealed class IgnorePropertyAttribute : Attribute
    {
        public bool All { get; set; }
        public bool Select { get; set; }
        public bool Insert { get; set; }
        public bool Update { get; set; }
        public bool Delete { get; set; }
        public bool Any { get; set; }
        public bool GetById { get; set; }

        public IgnorePropertyAttribute()
        {

        }
    }
}

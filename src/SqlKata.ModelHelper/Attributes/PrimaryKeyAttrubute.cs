﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SqlKata.ModelHelper
{
    [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public sealed class PrimaryKeyAttribute : Attribute
    {
        public bool AutoIncrement { get; }
        public PrimaryKeyAttribute(bool autoIncrement = true)
        {
            this.AutoIncrement = autoIncrement;
        }
    }
}

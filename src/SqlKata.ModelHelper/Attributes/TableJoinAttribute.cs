﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SqlKata.ModelHelper
{
    [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public sealed class TableJoinAttribute : Attribute
    {
        public TableJoinAttribute(string tableName, JoinType joinType, string leftKey, string rightKey)
        {
            this.TableName = tableName;
            this.Type = joinType;
            this.LeftKey = leftKey;
            this.RightKey = rightKey;
        }

        public string TableName { get; }
        public string LeftKey { get; }
        public string RightKey { get; }
        public JoinType Type { get; }
    }
}

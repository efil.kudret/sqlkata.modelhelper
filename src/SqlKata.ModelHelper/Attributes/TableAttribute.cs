﻿namespace SqlKata.ModelHelper
{
    using System;
    using System.Linq;

    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public sealed class TableAttribute : Attribute
    {
        public TableAttribute(string tableName)
        {
            this.TableName = tableName;
        }

        public string TableName { get; }
        public string Alias
        {
            get
            {
                if (TableName.IndexOf(" ") > 0)
                    return TableName.Split(" ").LastOrDefault();
                else
                    return "";
            }
        }
    }
}

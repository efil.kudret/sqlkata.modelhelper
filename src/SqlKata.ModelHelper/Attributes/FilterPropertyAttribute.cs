﻿namespace SqlKata.ModelHelper
{
    using System;

    [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public sealed class FilterPropertyAttribute : Attribute
    {
        public FilterType FilterType { get; }
        public string ColumnName { get; set; }

        //if is is true, query builder not add as a column. use only in where clouse.
        public bool OnlyUseAsFilter { get; set; }
        public string CustomDbType { get; set; }

        public FilterPropertyAttribute(FilterType filterType)
        {
            this.FilterType = filterType;
        }
    }
}

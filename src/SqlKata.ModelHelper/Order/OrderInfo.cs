﻿namespace SqlKata.ModelHelper.Order
{
    public class OrderInfo
    {
        public string OrderKey { get; set; }
        public bool Desc { get; set; }

        public OrderInfo()
        {
            
        }

        public OrderInfo(string orderKey, bool desc = false)
        {
            OrderKey = orderKey;
            Desc = desc;
        }
    }
}
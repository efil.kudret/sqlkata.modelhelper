using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Npgsql;
using SqlKata.ModelHelper.DBRepository.Config;
using SqlKata.ModelHelper.Parameter;

namespace SqlKata.ModelHelper.DBRepository
{
    public class PgDbRepository
    {
        private readonly DbConfig _dbConfig;
        private readonly ILogger<PgDbRepository> _logger;

        public PgDbRepository(DbConfig config,  ILogger<PgDbRepository> logger)
        {
            _logger = logger;
            _dbConfig = config;
        }
        
        public async Task Execute(string procedure, PgParam parameter)
        {
            try
            {
                await using var db = new NpgsqlConnection(_dbConfig.ConnectionString);
                await db.OpenAsync();
                await db.ExecuteAsync(procedure, parameter, commandType: CommandType.StoredProcedure);
            }
            catch (NpgsqlException ex)
            {
                _logger.LogError("Execute Error on DB");
                _logger.LogError(ex.Message, ex);
                throw;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Execute Error");
                throw new Exception("UNKNOWN_ERROR");
            }
        }

        public async Task Execute(string procedure)
        {
            try
            {
                await using var db = new NpgsqlConnection(_dbConfig.ConnectionString);
                await db.OpenAsync();
                await db.ExecuteAsync(procedure, commandType: CommandType.StoredProcedure);
            }
            catch (NpgsqlException ex)
            {
                _logger.LogError("Execute Error on DB");
                _logger.LogError(ex.Message, ex);
                throw;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Execute Error");
                throw new Exception("System Error");
            }
        }

        public async Task<IEnumerable<T>> Query<T>(string procedure)
        {
            try
            {
                await using var db = new NpgsqlConnection(_dbConfig.ConnectionString);
                await db.OpenAsync();
                return await db.QueryAsync<T>(procedure, commandType: CommandType.StoredProcedure);
            }
            catch (NpgsqlException ex)
            {
                _logger.LogError("Execute Error on DB");
                _logger.LogError(ex.Message, ex);
                throw;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Query Error");
                throw new Exception("System Error");
            }
        }

        public async Task<IEnumerable<T>> Query<T>(string procedure, PgParam parameter)
        {
            try
            {
                await using var db = new NpgsqlConnection(_dbConfig.ConnectionString);
                await db.OpenAsync();
                return await db.QueryAsync<T>(procedure, parameter, commandType: CommandType.StoredProcedure);
            }
            catch (NpgsqlException ex)
            {
                _logger.LogError("Execute Error on DB");
                _logger.LogError(ex.Message, ex);
                throw;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Query Error");
                throw new Exception("System Error");
            }
        }

        public async Task<T> QuerySingle<T>(string procedure, PgParam parameter)
        {
            try
            {
                await using var db = new NpgsqlConnection(_dbConfig.ConnectionString);
                await db.OpenAsync();
                return await db.QuerySingleOrDefaultAsync<T>(procedure, parameter,
                    commandType: CommandType.StoredProcedure);
            }
            catch (NpgsqlException ex)
            {
                _logger.LogError("Execute Error on DB");
                _logger.LogError(ex.Message, ex);
                throw;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Query Single Error");
                throw new Exception("System Error");
            }
        }

        public async Task<T> QuerySingle<T>(string procedure)
        {
            try
            {
                await using var db = new NpgsqlConnection(_dbConfig.ConnectionString);
                await db.OpenAsync();
                return await db.QuerySingleOrDefaultAsync<T>(procedure, commandType: CommandType.StoredProcedure);
            }
            catch (NpgsqlException ex)
            {
                _logger.LogError("Execute Error on DB");
                _logger.LogError(ex.Message, ex);
                throw;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Query Single Error");
                throw new Exception("System Error");
            }
        }

        public async Task<T> RawQuery<T>(string query, PgParam parameter)
        {
            try
            {
                await using var db = new NpgsqlConnection(_dbConfig.ConnectionString);
                await db.OpenAsync();
                return await db.QuerySingleOrDefaultAsync<T>(query, parameter, commandType: CommandType.Text);
            }
            catch (NpgsqlException ex)
            {
                _logger.LogError("Execute Error on DB");
                _logger.LogError(ex.Message, ex);
                throw;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Query Single Error");
                throw new Exception("System Error");
            }
        }
    }
}
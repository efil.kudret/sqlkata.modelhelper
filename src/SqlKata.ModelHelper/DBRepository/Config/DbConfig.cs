namespace SqlKata.ModelHelper.DBRepository.Config
{
    public class DbConfig
    {
        public string ConnectionString { get; set; }
    }
}
using SqlKata.ModelHelper.Order;

namespace SqlKata.ModelHelper
{
    using SqlKata;
    using SqlKata.Execution;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Dynamic;
    using System.Linq;
    using System.Reflection;
    using NRTech.Toolkit;
    using NRTech.Toolkit.Data;

    public static class TableHelperExtensions
    {
        public static string ToUnderscoreCase(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return "";
            return string.Concat(str.Select((x, i) =>
                i > 0 && char.IsUpper(x) ? "_" + x.ToString().ToLowerInvariant() : x.ToString().ToLowerInvariant()));
        }

        public static string GetTableName(this ITable obj)
        {
            var attr = obj.GetType().GetCustomAttribute<TableAttribute>(false);
            if (attr is null)
                throw new NotSupportedException();
            else
                return attr.TableName;
        }

        public static string GetTableNameWithoutAlias(this ITable obj)
        {
            var attr = obj.GetType().GetCustomAttribute<TableAttribute>(false);
            if (attr is null)
                throw new NotSupportedException();
            else
                return attr.TableName.ToLowerInvariant().Split(" as ").FirstOrDefault();
        }

        public static Query GetInsertQuery(this ITable obj)
        {
            if (obj is IEnumerable)
                throw new NotImplementedException();


            List<string> columns = new List<string>();
            List<object> values = new List<object>();

            foreach (var item in FieldInfo.GetAllFields(obj))
            {
                if (item.PrimaryKey && item.AutoIncrement)
                    continue;

                if (!item.AlwaysInQuery &&
                    (item.Value.IsNull() || item.AutoIncrement || item.IgnoredFor(QueryTypes.Insert)))
                    continue;

                if (item.DataColumn.IsNotNull() && item.DataColumn.Foreign)
                    continue;

                columns.Add(item.FieldNameOnly);

                if (item.Value is ExpandoObject)
                    values.Add((item.Value as ExpandoObject).ToRawData());
                else
                    values.Add(item.Value);
            }

            return new Query(obj.GetTableNameWithoutAlias()).AsInsert(columns, new[] { values });
        }

        public static FieldInfo GetPrimaryKey(this ITable obj)
        {
            var fields = FieldInfo.GetAllFields(obj);

            var primary = fields.FirstOrDefault(x => x.PrimaryKey);
            if (primary.IsNotNull())
                return primary;
            else
                return null;
        }

        public static Query GetUpdateQuery(this ITable obj, params string[] updateOnlyFields)
        {
            if (obj is IEnumerable)
                throw new NotImplementedException();

            Dictionary<string, object> dic = new Dictionary<string, object>();
            var fields = FieldInfo.GetAllFields(obj);


            IEnumerable<FieldInfo> filtered;

            if (updateOnlyFields.IsNotNull() && updateOnlyFields.Length > 0)
                filtered = fields.Where(x => updateOnlyFields.Contains(x.PropertyName));
            else
                filtered = fields.Where
                (
                    x =>
                        !x.IgnoredFor(QueryTypes.Update)
                        && (x.AlwaysInQuery || x.Value.IsNotDefault())
                        && !x.AutoIncrement
                );

            foreach (var item in filtered)
                dic.Add(item.FieldNameOnly, item.Value);

            var primaryKeys = fields.Where(x => x.PrimaryKey);
            var query = new Query(obj.GetTableName());
            foreach (var item in primaryKeys)
                query = query.Where(item.FieldName, item.Value);

            return query.AsUpdate(dic);
        }

        public static Query GetDeleteQuery(this ITable obj)
        {
            if (obj is IEnumerable)
                throw new NotImplementedException();
            var query = new Query(obj.GetTableName());

            var primaryFields = FieldInfo.GetAllFields(obj).Where(x => x.PrimaryKey);

            if (!primaryFields.Any())
                throw new NotSupportedException("Object must have mininum one primary key.");

            foreach (var item in primaryFields)
                if ((item.Filter.IsNotNull() && item.Filter.FilterType == FilterType.Ignore))
                    continue;
                else
                    query = query.Where(item.FieldName, item.Value);

            return query.AsDelete();
        }

        public static object GetInsertData(ITable obj)
        {
            var data = new System.Dynamic.ExpandoObject();
            foreach (var item in FieldInfo.GetAllFields(obj))
                ((IDictionary<String, Object>)data).Add(item.Underscored, item.Value);
            return data;
        }

        public static Query GetSelectQuery(this ITable obj, SelectQueryOptions opts = null)
        {
            try
            {
                if (opts is null)
                {
                    opts = new SelectQueryOptions
                    {
                        HasDeletedField = true
                    };
                }

                var attr = obj.GetType().GetCustomAttribute<TableAttribute>(false);
                if (attr is null)
                    throw new NotSupportedException();
                else
                {
                    var fields = FieldInfo.GetAllFields(obj);
                    var q = new Query(attr.TableName);

                    if (opts.HasDeletedField)
                        q = q.Where((!string.IsNullOrEmpty(attr.Alias) ? attr.Alias + "." : "") + "deleted", false);

                    if (opts.ForGetById)
                        foreach (var item in fields.Where(x => x.PrimaryKey))
                            q = q.Where(item.FieldName, item.Value);

                    foreach (var field in fields.Where(x => x.TableJoin.IsNotNull()))
                        switch (field.TableJoin.Type)
                        {
                            default:
                            case JoinType.Inner:
                                q = q.Join(field.TableJoin.TableName, field.TableJoin.LeftKey,
                                    field.TableJoin.RightKey);
                                break;
                            case JoinType.Left:
                                q = q.LeftJoin(field.TableJoin.TableName, field.TableJoin.LeftKey,
                                    field.TableJoin.RightKey);
                                break;
                            case JoinType.Right:
                                q = q.RightJoin(field.TableJoin.TableName, field.TableJoin.LeftKey,
                                    field.TableJoin.RightKey);
                                break;
                        }

                    if (opts.ValuesForFilter)
                    {
                        foreach (var field in fields.Where(x => (!x.IsNull && !x.IsDefault) || x.IsFilterType))
                        {
                            if (field.IgnoredFor(QueryTypes.Select))
                                continue;

                            if (field.IsFilterType)
                            {
                                switch (field.Filter.FilterType)
                                {
                                    case FilterType.DateRange:
                                        if (field.Value.IsDefault())
                                            continue;
                                        DateTime? date1, date2;
                                        field.Value.ToString().GetDates(out date1, out date2);
                                        if (date1.HasValue)
                                            q = q.Where(field.Filter.ColumnName, ">=", date1.StartOfDay());

                                        if (date2.HasValue)
                                            q = q.Where(field.Filter.ColumnName, "<=", date2.EndOfDay());
                                        break;
                                    case FilterType.IntRange:
                                    case FilterType.DecimalRange:
                                        if (field.Value.IsDefault())
                                            continue;
                                        decimal? numeric1, numeric2;
                                        field.Value.ToString().GetNumericRanges(out numeric1, out numeric2);
                                        if (numeric1.HasValue)
                                            q = q.Where(field.Filter.ColumnName, ">=", numeric1);
                                        if (numeric2.HasValue)
                                            q = q.Where(field.Filter.ColumnName, "<=", numeric2);
                                        break;
                                    case FilterType.OrNull:
                                        var fieldName = field.Filter.ColumnName.IsNotDefault()
                                            ? field.Filter.ColumnName
                                            : field.FieldNameWithoutAlias;
                                        q = q.Where(sq => sq.Where(fieldName, field.Value).OrWhere(fieldName, null));
                                        break;
                                    case FilterType.Equal:
                                        var name = field.Filter.ColumnName.IsNotDefault()
                                            ? field.Filter.ColumnName
                                            : field.FieldNameWithoutAlias;
                                        q = q.Where(name, field.Value);
                                        break;
                                    case FilterType.GratherThan:
                                        var gFieldName = field.Filter.ColumnName.IsNotDefault()
                                            ? field.Filter.ColumnName
                                            : field.FieldNameWithoutAlias;
                                        q = q.Where(gFieldName, ">=", field.Value);
                                        break;
                                    case FilterType.LowerThan:
                                        var lFieldName = field.Filter.ColumnName.IsNotDefault()
                                            ? field.Filter.ColumnName
                                            : field.FieldNameWithoutAlias;
                                        q = q.Where(lFieldName, "<=", field.Value);
                                        break;
                                }
                            }
                            else
                            {
                                if (!opts.UseOrWhere)
                                {
                                    if (field.Value.IsNotDefault() && field.Value is string && !opts.FiltersForEqual)
                                        q = q.WhereContains(field.FieldNameWithoutAlias, field.Value);
                                    else
                                        q = q.Where(field.FieldNameWithoutAlias, field.Value);
                                }
                                else
                                {
                                    if (field.Value.IsNotDefault() && field.Value is string && !opts.FiltersForEqual)
                                        q = q.OrWhereContains(field.FieldNameWithoutAlias, field.Value);
                                    else
                                        q = q.OrWhere(field.FieldNameWithoutAlias, field.Value);
                                }
                            }
                        }
                    }

                    var keys = fields.Where(x => !x.IgnoredFor(QueryTypes.Select) && !x.SelectRaw
                            && (x.Filter.IsNull() || !x.Filter.OnlyUseAsFilter))
                        .Select(x => x.FieldName);

                    if (obj is IHaveCustomTableJoin)
                        q = ((IHaveCustomTableJoin)obj).GetCustomTableJoins(q);

                    var query = q.Select(keys.ToArray());

                    if (!fields.Any(x => x.DataColumn.IsNotNull() && x.DataColumn.SelectRaw)) return query;
                    {
                        foreach (var item in fields.Where(x => x.DataColumn.IsNotNull() && x.DataColumn.SelectRaw))
                            query = query.SelectRaw(item.FieldName);
                    }

                    return query;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw e;
            }
        }

        public static Query GetSelectQuery(this ITable obj, IFilterObject filter, OrderInfo orderInfo,
            SelectQueryOptions opts = null)
        {
            try
            {
                if (opts is null)
                {
                    opts = new SelectQueryOptions
                    {
                        HasDeletedField = true
                    };
                }

                var attr = obj.GetType().GetCustomAttribute<TableAttribute>(false);
                if (attr is null)
                    throw new NotSupportedException();

                var fields = FieldInfo.GetAllFields(filter);
                var q = new Query(attr.TableName);

                q = AddDeleted(opts, q, attr);
                q = AddGetById(opts, fields, q);
                q = AddTableJoins(fields, q);

                if (opts.ValuesForFilter)
                {
                    var filters = FieldInfo.GetAllFields(filter);
                    foreach (var field in filters.Where(x => (!x.IsNull && !x.IsDefault) || x.IsFilterType))
                    {
                        if (field.IgnoredFor(QueryTypes.Select))
                            continue;

                        if (field.IsFilterType)
                        {
                            switch (field.Filter.FilterType)
                            {
                                case FilterType.DateRange:
                                    if (field.Value.IsDefault())
                                        continue;

                                    DateTime? date1, date2;
                                    field.Value.ToString().GetDates(out date1, out date2);
                                    if (date1.HasValue)
                                        q = q.Where(field.Filter.ColumnName, ">=", date1.StartOfDay());

                                    if (date2.HasValue)
                                        q = q.Where(field.Filter.ColumnName, "<=", date2.EndOfDay());
                                    break;
                                case FilterType.IntRange:
                                case FilterType.DecimalRange:
                                    if (field.Value.IsDefault())
                                        continue;

                                    field.Value.ToString().GetNumericRanges(out var numeric1, out var numeric2);
                                    if (numeric1.HasValue)
                                        q = q.Where(field.Filter.ColumnName, ">=", numeric1);
                                    if (numeric2.HasValue)
                                        q = q.Where(field.Filter.ColumnName, "<=", numeric2);
                                    break;
                                case FilterType.OrNull:
                                    var fieldName = !string.IsNullOrEmpty(field.Filter.ColumnName)
                                        ? field.Filter.ColumnName
                                        : field.FieldNameWithoutAlias;
                                    q = q.Where(sq => sq.Where(fieldName, field.Value).OrWhere(fieldName, null));
                                    break;
                                case FilterType.Equal:
                                    var name = !string.IsNullOrEmpty(field.Filter.ColumnName)
                                        ? field.Filter.ColumnName
                                        : field.FieldNameWithoutAlias;
                                    q = q.Where(name, field.Value);
                                    break;
                                case FilterType.GratherThan:
                                    var gFieldName = !string.IsNullOrEmpty(field.Filter.ColumnName)
                                        ? field.Filter.ColumnName
                                        : field.FieldNameWithoutAlias;
                                    q = q.Where(gFieldName, ">=", field.Value);
                                    break;
                                case FilterType.LowerThan:
                                    var lFieldName = !string.IsNullOrEmpty(field.Filter.ColumnName)
                                        ? field.Filter.ColumnName
                                        : field.FieldNameWithoutAlias;
                                    q = q.Where(lFieldName, "<=", field.Value);
                                    break;
                                case FilterType.CustomDbType:
                                    if (field.Value.IsNotDefault())
                                    {
                                        var cFieldName = !string.IsNullOrEmpty(field.Filter.ColumnName)
                                            ? field.Filter.ColumnName
                                            : field.FieldNameWithoutAlias;
                                        if (field.Filter.CustomDbType.IsDefault())
                                            throw new InvalidOperationException(
                                                "CustomDbType cannot found on FilterType attribute");
                                        q = q.WhereRaw($"{cFieldName}=?",
                                            $"'{field.Value}'::{field.Filter.CustomDbType}");
                                    }

                                    break;
                            }
                        }
                        else
                        {
                            if (!opts.UseOrWhere)
                            {
                                if (field.Value.IsNotDefault() && field.Value is string && !opts.FiltersForEqual)
                                    q = q.WhereContains(field.FieldNameWithoutAlias, field.Value);
                                else
                                    q = q.Where(field.FieldNameWithoutAlias, field.Value);
                            }
                            else
                            {
                                if (field.Value.IsNotDefault() && field.Value is string && !opts.FiltersForEqual)
                                    q = q.OrWhereContains(field.FieldNameWithoutAlias, field.Value);
                                else
                                    q = q.OrWhere(field.FieldNameWithoutAlias, field.Value);
                            }
                        }
                    }
                }


                var selectFields = FieldInfo.GetAllFields(obj);
                var keys = selectFields.Where(x => !x.IgnoredFor(QueryTypes.Select) && !x.SelectRaw
                        && (x.Filter.IsNull() || !x.Filter.OnlyUseAsFilter))
                    .Select(x => x.FieldName);

                var query = q.Select(keys.ToArray());
                if (fields.Any(x => x.DataColumn.IsNotNull() && x.DataColumn.SelectRaw))
                    foreach (var item in fields.Where(x => x.DataColumn.IsNotDefault() && x.DataColumn.SelectRaw))
                        query = query.SelectRaw(item.FieldName);


                if (filter is IHaveCustomTableJoin)
                    query = ((IHaveCustomTableJoin)filter).GetCustomTableJoins(query);


                if (orderInfo.IsNotNull())
                {
                    var k = orderInfo.OrderKey.ToPascalCase();
                    var field = fields.FirstOrDefault(x => x.PropertyName == k);
                    if (field.IsNotNull())
                    {
                        if (field.SelectRaw)
                            query = query.OrderByRaw($"{field.FieldName} {(orderInfo.Desc ? "desc" : "")}");
                        else
                        {
                            orderInfo.OrderKey = field.FieldNameWithoutAlias;
                            query = orderInfo.Desc
                                ? query.OrderByDesc(orderInfo.OrderKey)
                                : query.OrderBy(orderInfo.OrderKey);       
                        }
                    }
                }

                return query;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw e;
            }
        }

        private static Query AddTableJoins(IEnumerable<FieldInfo> fields, Query q)
        {
            foreach (var field in fields.Where(x => x.TableJoin.IsNotNull()))
                switch (field.TableJoin.Type)
                {
                    default:
                    case JoinType.Inner:
                        q = q.Join(field.TableJoin.TableName, field.TableJoin.LeftKey,
                            field.TableJoin.RightKey);
                        break;
                    case JoinType.Left:
                        q = q.LeftJoin(field.TableJoin.TableName, field.TableJoin.LeftKey,
                            field.TableJoin.RightKey);
                        break;
                    case JoinType.Right:
                        q = q.RightJoin(field.TableJoin.TableName, field.TableJoin.LeftKey,
                            field.TableJoin.RightKey);
                        break;
                }

            return q;
        }

        private static Query AddGetById(SelectQueryOptions opts, IEnumerable<FieldInfo> fields, Query q)
        {
            if (opts.ForGetById)
                foreach (var item in fields.Where(x => x.PrimaryKey))
                    q = q.Where(item.FieldName, item.Value);
            return q;
        }

        private static Query AddDeleted(SelectQueryOptions opts, Query q, TableAttribute attr)
        {
            if (opts.HasDeletedField)
                q = q.Where((!string.IsNullOrEmpty(attr.Alias) ? attr.Alias + "." : "") + "deleted", false);
            return q;
        }

        public static T CompileAndReturnId<T>(this Query query, QueryFactory factory, string primaryKeyName = "id")
        {
            var alias = primaryKeyName != "id" ? " as id" : "";

            var compiled = factory.Compiler.Compile(query);
            var sql = compiled.Sql + $" RETURNING {primaryKeyName} {alias};";
            if (LoggerOptions.SqlQueryLogs)
                Console.WriteLine(sql);
            return (T)factory.Select(sql, compiled.NamedBindings).FirstOrDefault()?.id;
        }

        public static void GetDates(this string dateRange, out DateTime? startDate, out DateTime? endDate)
        {
            startDate = null;
            endDate = null;
            if (string.IsNullOrEmpty(dateRange)) return;
            var dates = dateRange.Split(',').Select(DateTime.Parse).ToList();
            startDate = dates.FirstOrDefault();
            endDate = dates.LastOrDefault();
        }

        public static void SetDates(this string dateRange, DateTime startDate, DateTime endDate)
        {
            dateRange = $"{startDate.ToString("O")},{endDate.ToString("O")}";
        }

        public static void GetNumericRanges(this string numericRange, out decimal? start, out decimal? end)
        {
            start = null;
            end = null;
            if (string.IsNullOrEmpty(numericRange)) return;
            var decimals = numericRange.Split("___");
            if (decimals.Length >= 1 && decimal.TryParse(decimals[0], out var s))
                start = s;
            if (decimals.Length >= 2 && decimal.TryParse(decimals[1], out var e))
                end = e;
        }

        public static DateTime StartOfDay(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, 0, 0, 0);
        }

        public static DateTime StartOfDay(this DateTime? date)
        {
            return date.Value.StartOfDay();
        }

        public static DateTime EndOfDay(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, 23, 59, 59);
        }

        public static DateTime EndOfDay(this DateTime? date)
        {
            return date.Value.EndOfDay();
        }
    }
}
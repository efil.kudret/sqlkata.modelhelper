namespace SqlKata.ModelHelper
{
    public static class StringExtensions
    {
        public static string ToPascalCase(this string s)
        {
            return char.ToUpper(s[0]) + s.Substring(1);
        }
    }
}